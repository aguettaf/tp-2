
public class Patient {
	
//les attributs encasulé
	private String nom;
	private String prenom;
	private String médecinTraitant;
	private int numéoDeSécuritéSocial;
	private String dateDeSonDernierRV;
	private String dateDeSonProchainRV;
			
			//le constructeur:
	public Patient (String nom, String prenom,String médecinTraitant,int numéoDeSécuritéSocial, String dateDeSonDernierRV, String dateDeSonProchainRV) {
		this.nom=nom;
		this.prenom=prenom;
		this.médecinTraitant=médecinTraitant;
		this.numéoDeSécuritéSocial=numéoDeSécuritéSocial;
		this.dateDeSonDernierRV=dateDeSonDernierRV;
		this.dateDeSonProchainRV=dateDeSonProchainRV;
	}
			
			//les getters
		public String getnom() {
			return this.nom;
		}
		public String getprenom() {
			return this.prenom;
		}
		public String getmédecinTraitant() {
			return this.médecinTraitant;
		}
			public int numéoDeSécuritéSocial() {
				return this.numéoDeSécuritéSocial;
			}
			public String getdateDeSonDernierRV() {
				return this.dateDeSonDernierRV;
			}
			public String getdateDeSonProchainRV() {
				return this.dateDeSonProchainRV;
			}
			//les setters
			public void setnom(String nom) {
				this.nom=nom;
			}
			public void setprenom(String prenom) {
				this.prenom=prenom;
			}
			public void setmédecinTraitant(String médecinTraitant) {
				this.médecinTraitant=médecinTraitant;
			}
			public void setnuméoDeSécuritéSocial(int numéoDeSécuritéSocial) {
				this.numéoDeSécuritéSocial=numéoDeSécuritéSocial;
			}
			public void setdateDeSonDernierRV(String dateDeSonDernierRV) {
				this.dateDeSonDernierRV=dateDeSonDernierRV;
			}
			public void setdateDeSonProchainRV(String dateDeSonProchainRV) {
				this.dateDeSonProchainRV=dateDeSonProchainRV;
			}
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
