
public class Medecin_exo_2 {
	
		private String nom;
		private String adresse;
		private int numéroDeTéléphone;
		private int numéoAuRegistreDeLordre;
			
	//le constructeur
			
		public Medecin_exo_2 (String nom, String adresse, int numéroDeTéléphone, int numéoAuRegistreDeLordre) {
			this.nom=nom;
			this.adresse=adresse;
			this.numéroDeTéléphone=numéroDeTéléphone;
			this.numéoAuRegistreDeLordre= numéoAuRegistreDeLordre;
		}
	//les getters
		public String getnom() {
			return this.nom;
		}
		public String getadresse() {
			return this.adresse;
		}
		public int getnuméroDeTéléphone() {
			return this.numéroDeTéléphone;
		}
		public int getnuméoAuRegistreDeLordre() {
			return this.numéoAuRegistreDeLordre;
		}
	//les setters
		public void setnom (String nom) {
			this.nom=nom;
		}
		public void setadresse(String adresse) {
			this.adresse=adresse;
		}
		public void setnuméroDeTéléphone(int numéroDeTéléphone) {
			this.numéroDeTéléphone=numéroDeTéléphone;
		}
		public void setnuméoAuRegistreDeLordre(int numéoAuRegistreDeLordre) {
			this.numéoAuRegistreDeLordre=numéoAuRegistreDeLordre;
		}	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
